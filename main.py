# -- coding: utf-8 --
# @File : main.py
# @Software : PyCharm
# @Time : 2022/5/17 2:00 下午

from fastapi import FastAPI,Query,Cookie,Header,status,HTTPException

app = FastAPI()

@app.get("/")
async def root():
    return {"message": "Hello World"}

"""参数接受类型"""
@app.get("/items/{item_id}")
async def read_item(item_id: int):
    return {"item_id": item_id}

"""接口顺序，me在{user_id}之前"""
@app.get("/users/me")
async def read_user_me():
    return {"user_id": "the current user"}

@app.get("/users/{user_id}")
async def read_user(user_id: str):
    return {"user_id": user_id}

from enum import Enum

"""预设值,枚举类型"""
class Model(str,Enum):
    name = "SHUMIN"
    sex  = "MAN"

@app.get("/models/{model_name}")
async def get_model(model_name: Model):
        if model_name == Model.name:
            return {'model_name':model_name}
        if model_name.value == "SHUMIN":
            return {'model_name_value',model_name.value}
        return {'model_name':'None'}


"""(路径参数) 接口对应的请求：问 http://127.0.0.1:8000/items/{parameter}"""

"""
查询参数: http://127.0.0.1:8000/items/?skip=0&limit=10
"""
@app.get("/items/")
async def read_items(skip: int = 0,limit: int = 10):
    fake_items_db = [{"item_name": "Foo"}, {"item_name": "Bar"}, {"item_name": "Baz"}]
    # fake_items_db[0:10]
    return fake_items_db[skip: skip +limit]

from typing import Union,List

"""可选参数: http://127.0.0.1:8000/items/1?short=true"""
@app.get("/items_option/{item_id}")
async def get_items(item_id: int,q:Union[str,None] = None,short: bool = False):
    if q:
        return {"item_id":item_id,"q":q,"short":short}
    return {"item_id":item_id,"short":short}


"""多路径查询参数："""
@app.get("/user/{user_id}/items/{item_id}")
async def read_user_item(user_id: int,item_id: int,q: Union[str, None] = None, short: bool = False):
    item = {"item_id": item_id, "owner_id": user_id}
    if q:
        item.update({"q": q})
    if not short:
        item.update({"short":short})
    return item


"""必须查询参数"""
@app.get("/need/{need_id}")
async def get_need(need_id: str,needy: str):
    item = {"item_id": need_id, "needy": needy}
    return item

from pydantic import BaseModel,EmailStr

class Items(BaseModel):
    name: str
    desc: Union[str,None] = None
    price: float
    tax: Union[float,None] = None

"""json请求体: Pydantic"""
@app.post("/json")
async def get_json(item: Items):
    return item


"""json请求体+参数"""
@app.put("/items/{item_id}")
async def put_json(item_id: int,item: Items):
    return {"item_id": item_id, **item.dict()}



"""参数校验：Query"""
@app.get("/check/")
async def check_item(q:Union[str,None] = Query(default=None,max_length=3)):
    results = {"items": [{"item_id": "Foo"}, {"item_id": "Bar"}]}
    if q:
        results.update({"q":q})
    return results

class Tags(BaseModel):
    name: str
    description: Union[str, None] = None
    price: float
    tax: Union[float, None] = None
    tags: List[str] = []

"""返回值模型"""
@app.post("/man/",response_model=Tags)
async def post_man(tags: Tags):
    return tags

"""返回值模型"""
class UserIn(BaseModel):
    username: str
    password: str
    email: EmailStr
    full_name: Union[str, None] = None


class UserOut(BaseModel):
    username: str
    email: EmailStr
    full_name: Union[str, None] = None


@app.post("/user/", response_model=UserOut)
async def create_user(user: UserIn):
    return user


"""响应状态码"""
@app.post("/items_status/", status_code=status.HTTP_200_OK)
async def create_item(name: str):
    return {"name": name}

"""错误处理"""
@app.get("/items/{item_id}")
async def read_item(item_id: str):
    items = {"foo": "The Foo Wrestlers"}
    if item_id not in items:
        raise HTTPException(status_code=404, detail="Item not found")
    return {"item": items[item_id]}


"""概要： https://fastapi.tiangolo.com/zh/tutorial/path-operation-configuration/
生成swagger文件的描述和介绍
"""







