# holidays

简介：主要使用python实现假期日历功能。基于 [fastapi框架](https://fastapi.tiangolo.com/zh/advanced/additional-status-codes/) 。

## [安装本地依赖](https://pyloong.github.io/pythonic-project-guidelines/)

- 生成依赖文件

```shell
pip install uvicorn[standard]
pip install -r requirements.txt
pip freeze > requirements.txt # 虚拟环境
```

- 安装依赖
    - conda-forge：源地址 conda-forge
```shell
conda install -c conda-forge workalendar
conda install -c conda-forge fastapi 
```

- 启动服务
  - [swagger: http://127.0.0.1:8000/docs](http://127.0.0.1:8000/docs)
  - [OpenAPI: http://127.0.0.1:8000/openapi.json](http://127.0.0.1:8000/openapi.json)
  - [Redoc: http://127.0.0.1:8000/redoc](http://127.0.0.1:8000/redoc)
```shell
uvicorn main:app --reload
```


## 基础知识介绍

#### 参数命令行解析 (argparse)

- [test/args.py](test/args.py)


#### 读取配置文件 (ConfigParser)

- [test/cfg.py](test/cfg.py)

#### Pass使用示例 (pass)

- [test/pass.py](test/pass.py)

#### *args与**kwargs区别 

- [test/kwargs.py](test/kwargs.py)

#### 数据类型定义和方法

- [test/base.py](test/base.py)

#### setup介绍
- [setup.py介绍](https://www.jianshu.com/p/9a5e7c935273)


