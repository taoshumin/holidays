# -- coding: utf-8 --
# @File : base.py
# @Software : PyCharm
# @Time : 2022/5/17 2:23 下午

"""
copy from https://docs.python.org/zh-cn/3/library/typing.html
"""
from typing import (
    Any,
    Dict,
    Iterable,
    List,
    Optional,
    Set,
    TYPE_CHECKING,
    Tuple,
    Union,
)


class data(object):
    pass


class Base(object):
    """定义参数：country str数据类型"""
    country: str
    """定义参数：subs 列表数据类型"""
    subs: List[str] = []
    """定义参数： bool类型"""
    isSelect: bool
    """定义可选类型：可选类型只能写一种数据类型（类似于默认值）"""
    opt: Optional[str]
    """Union：参数必须是某种类型，且至少有一个"""
    years: Union[str,int] = None

    def __init__(self,
                 country: str,
                 years: Union[str, int] = None,
                 subs: List[str] = None,
                 opt: Optional[str] = None,
                 isSelect: bool = True) -> None:
        super.__init__()
        self.country = country
        self.subs = subs
        self.isSelect = isSelect
        self.opt = opt

    """返回值：str类型"""

    def name(self) -> str:
        return self.country

    """返回值：None"""

    def append(self) -> None:
        pass

    """返回值： 切片"""

    def pop(self) -> [data]:
        list = [data()]
        return list

    """Optional"""
    # def foo_func(arg: int = None):
    # def foo_func(arg: Optional[int] = None):
    # 没有区别
    """Union"""

    """类型判断：isinstance"""
    # if isinstance(years, int):