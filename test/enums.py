# -- coding: utf-8 --
# @File : enums.py
# @Software : PyCharm
# @Time : 2022/5/17 3:44 下午

"""3.4之后Enum
copy from https://docs.python.org/3/library/enum.html
"""

from enum import Enum


class Color(Enum):
    RED = 1
    GREEN = 2
    BLUE = 3


print(Color.RED)
print(Color.RED.value)
