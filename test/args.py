# -- coding: utf-8 --
# @File : args.py
# @Software : PyCharm
# @Time : 2022/5/17 10:24 上午

import argparse

"""
解析命令行
"""


def get_parser():
    """
    1. 导入模块
    2. 创建解析对象
    3. 添加解析参数
    4. 进行解析
    :return 解析
    """
    parser = argparse.ArgumentParser(description="Demo of argparse")
    # 设置默认值
    parser.add_argument("-n", "--name", default='SHUMIN', help="请输入名称。默认值（SHUMIN）")
    # 指定类型,必要参数
    parser.add_argument("-o", "--out", type=str, help="请输入输出文件地址。", required=True)
    # 指定int类型
    parser.add_argument('-i', "--int", type=int, help="请输入INT类型。")
    # 添加多个字段
    parser.add_argument("-c", "--count", nargs='+')  #
    return parser


"""
N   参数的绝对个数（例如：3）
'?'   0或1个参数
'*'   0或所有参数
'+'   所有，并且至少一个参数

"""

if __name__ == '__main__':
    parser = get_parser()
    args = parser.parse_args()
    name = args.name
    out = args.out
    it = args.int
    counts = ','.join(args.count)
    print('{}'.format(name))
    print('{}'.format(out))
    print('{}'.format(it))
    print('{}'.format(counts))

"""
使用方式:
python args.py --help
python args.py -n CHENXUE
python args.py -c A B C -o "1.txt"
"""
