# -- coding: utf-8 --
# @File : pass.py
# @Software : PyCharm
# @Time : 2022/5/17 2:06 下午

"""
pass占位符，类似于起别名
比如中国可以使用China，也可以使用CN，或者使用HK
"""

class China(object):
    def __init__(self,name: str):
        self.name = name

class CN(China):
    pass

class HK(China):
    pass