# -- coding: utf-8 --
# @File : kwargs.py
# @Software : PyCharm
# @Time : 2022/5/17 2:14 下午

"""
*args与**kwargs区别
"""


"""
*args: 切片
"""
def test_args(*args):
    for arg in args:
        print('{}'.format(arg))


"""
**kwargs： 字典
"""
def test_kwargs(**kwargs):
    if kwargs is not None:
        for key, value in kwargs.items():
            print('{} = {}'.format(key,value))


if __name__ == '__main__':
    test_args('python','java','Golang')
    test_kwargs(name="python", value=5)