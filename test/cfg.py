# -- coding: utf-8 --
# @File : cfg.py
# @Software : PyCharm
# @Time : 2022/5/17 11:09 上午

# from 包名 import 类名
from configparser import ConfigParser

"""
解析配置文件 
copy from https://www.1024sou.com/article/360078.html
"""

def get_config(filename: str):
    conf = ConfigParser()
    conf.read(filename, encoding='utf-8')
    db = conf.items('DATBASE')
    print(db)

"""
写入配置文件
"""
def write_config(filname: str):
    conf = ConfigParser()
    conf["DEFAULT"] = {'ServerAliveInterval': '45',
                         'Compression': 'yes',
                         'CompressionLevel': '9',
                         'ForwardX11': 'yes'
                         }
    conf["DATBASE"] = {
        'Address': "127.0.0.1:2379",
        'TableName': 'CPS'
    }

    with open(filname,'w') as file:
        conf.write(file)


if __name__ == '__main__':
    # write_config('cps.ini')
    get_config('cps.ini')